require 'faraday'

module CollectiveCount

  class Client
    attr_reader :host, :version

    def initialize( host: 'http://counts.collectivebias.com' )
      @host = CollectiveCount.host || host
    end

    def get( endpoint, params={} )
      r = client.get endpoint, params
      r.body
    end

    def client
      @client ||= Faraday.new( url: host ) do |f|
        f.adapter Faraday.default_adapter
      end
    end

  end

end