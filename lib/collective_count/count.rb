require 'json'

module CollectiveCount

  class Count < OpenStruct

    def self.get( url )
      attrs = JSON.parse( CollectiveCount.client.get( "/", {url: url} ) )
      new(attrs)
    end

  end

end