require "collective_count/version"
require "collective_count/client"
require "collective_count/count"

module CollectiveCount
  class << self
    attr_accessor :host

    def client
      @client ||= Client.new
    end

    def configure
      yield self
      true
    end
  end
end
