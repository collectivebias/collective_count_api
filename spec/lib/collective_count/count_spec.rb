require 'collective_count'
CollectiveCount.host = "http://localhost:3000"

describe CollectiveCount::Count do
  let(:url) { "http://collectivebias.com" }

  describe '.get' do
    it 'returns a Count' do
      expect( CollectiveCount::Count.get(url) ).to be_a CollectiveCount::Count
    end
  end

  subject { CollectiveCount::Count.get(url) }

  it { should respond_to(:twitter)     }
  it { should respond_to(:facebook)    }
  it { should respond_to(:pinterest)   }
  it { should respond_to(:google_plus) }

end