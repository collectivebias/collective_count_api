# CollectiveCount

TODO: Write a gem description

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'collective_count', git: 'git@bitbucket.org:collectivebias/collective_count_api.git'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install collective_count

## Usage


```
#!ruby

# config/initializers/collective_count.rb
CollectiveCount.host = 'path/to/custom/server'

# Get shared counts for url
count = CollectiveCount::Count.get('some_url')
twitter = count.twitter
facebook = count.facebook
pinterest = count.pinterest
google_plus = count.google_plus
```


## Contributing

1. Fork it ( https://github.com/[my-github-username]/collective_count/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request