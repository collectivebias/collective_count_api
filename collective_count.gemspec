# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'collective_count/version'

Gem::Specification.new do |spec|
  spec.name          = "collective_count"
  spec.version       = CollectiveCount::VERSION
  spec.authors       = ["Andy Brown"]
  spec.email         = ["asbrown002@gmail.com", "andybrown@collectivebias.com"]
  spec.summary       = "Simple API wrapper for go project collective-count"
  spec.description   = "Simple API wrapper for go project collective-count"
  spec.homepage      = "http://collectivebias.com"
  spec.license       = "MIT"

  spec.files         = Dir.glob("lib/**/*.rb")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
  spec.add_runtime_dependency "faraday", "~> 0.9", ">= 0.9.0"
end
